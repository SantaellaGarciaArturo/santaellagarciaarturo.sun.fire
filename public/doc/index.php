<?php ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>API</title>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
				<div class="post">
						<h2 class="title">API Metodos</h2>
						<div class="entry">

<h3><a href="../">Regresar</a></h3>
		<h2>Obtener el Producto Interno Bruto por año</h2>
				<p>Descripción: El Producto Interno Bruto nacional por cada año </p>
<ul><li><strong>URL</strong>: http://santaellagarciaarturo.sun.fire/api/pib</li>
<li><strong>Método HTTP</strong>: GET</li>
<li><strong>Parámetros</strong>: <dd>ninguno</dd4></li>
 
      <li><strong>Presentación de resultado</strong>: XML</li>
      <dd>Resultado:</dd>
     </ul>
			<p align="left">
  <a>   
 &lt;desc_indicador&gt;Producto Interno Bruto estatal a precios constantes de 1993. Servicios de intermediación financiera medidos indirectamente&lt;/desc_indicador&gt;
 &lt;unidad_medida&gt;Miles de pesos&gt;&lt;/unidad_medida&gt;&lt;anio&gt;1994&lt;/anio
 &gt;&lt;tema_nivel_1&gt;Economia&lt;/tema_nivel_1&gt;&lt;tema_nivel_2&gt;Cuentas nacionales&lt;/tema_nivel_2&gt;&lt;tema_nivel_3&gt;Producto Interno Bruto&lt;/tema_nivel_3&gt;&gt;desc_indicador&gt;
 </a>     		
</p>

</li>
</ul>
</div>
 <div class="post">
<h2 class="title"></h2>
<div class="entry">
<h2>Obtener pib por año mediante un indicador</h2>
<p>Descripción: El pib de cada año por un indicador</p>
<ul><li><strong>URL</strong>: http://santaellagarciaarturo.sun.fire/api/pib/indicador</li>
<li><strong>Método HTTP</strong>: GET</li>
<li><strong>Parámetros</strong>:
<br><dd>indicador = identificador del indicador </dd>
</li>
<a>
	<li>
		<strong>respuesta incorrecta</strong>: si el parametro indicador es incorrecto se responde un mensage de estado HTTP 404 y un mensage de error: el indicador con el "id" no existe o no cumple con el formato correcto
	</li>	
	</a>
	<li><strong>Presentación de resultado</strong>: XML</li>
<dd>Resultado:</dd>

</ul>
<p align="left">
  <a>   
                  &lt;desc_indicador&gt;Producto Interno Bruto estatal a precios constantes de 1993. Servicios de intermediación financiera medidos indirectamente&lt;/desc_indicador&gt;&lt;cantidad&gt;-37435874&lt;/cantidad&gt;&lt;unidad_medida&gt;Miles de pesos&lt;/unidad_medida&gt;&lt;anio&gt;1994&lt;/anio&gt;&lt;tema_nivel_1&gt;Economia&lt;/tema_nivel_1&gt;&lt;tema_nivel_2&gt;Cuentas nacionales&lt;/tema_nivel_2&gt;&lt;tema_nivel_3&gt;Producto Interno Bruto&lt;/tema_nivel_3&gt;&lt;desc_indicador&gt;
              </a>     		
</p>
</li>
</ul>
</div>
<div class="post">
<h2 class="title"></h2>

<div class="entry">
<h2>Pib por clave de entidad</h2>
 <p> Descripción: Obtener el pib por cada clave de estado de todos los años</p>
	 <ul><li><strong>URL</strong>: http://santaellagarciaarturo.sun.fire/api/pib/entidad/clave</li>
 <li><strong>Método HTTP</strong>: GET</li>
 	<li><strong>Parámetros</strong>: <dd>clave = clave de la entidad. Ejemplo {00=nacional}</dd4></li>
 <li>
<a>
 	<strong>Respuesta</strong>: si el parametro indicador es incorrecto se responde un mensage de estado HTTP 404 y un mensage de error: el indicador con el id no existe o no cumple con el formato correcto
    </a>
      </li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <dd>Resultado:</dd>
     </ul>
<p>
<a>	
&lt;desc_indicador
&gt;Producto Interno Bruto estatal a precios constantes de 1993. Servicios de intermediación financiera medidos indirectamente&lt;/desc_indicador
&gt;&lt;unidad_medida
&gt;Miles de pesos&lt;/unidad_medida
&gt;&lt;anio
&gt;1994&lt;/anio
&gt;&lt;tema_nivel_1
&gt;Economia&lt;/tema_nivel_1
&gt;&lt;tema_nivel_2
&gt;Cuentas nacionales&lt;/tema_nivel_2
&gt;&lt;tema_nivel_3
&gt;Producto Interno Bruto&lt;/tema_nivel_3
&gt;&lt;desc_indicador
&gt;
</a>
</p>
</li>
</ul>
</div>


<div class="post">
<h2 class="title"></h2>

<div class="entry">
<h2>Agregar una nueva entidad</h2>
 <p>Descripción: Agrega una nueva entidad mediante el parametro desc indicado por la URL</p>
	 <ul><li><strong>URL</strong>: http://santaellagarciaarturo.sun.fire/api/entidad?desc="descripción"</li>
 <li><strong>Método HTTP</strong>: POST</li>
 	<li><strong>Parámetros</strong>: 
    <dd>entidad=identificador de la entidad</dd4></li>
 	<dd>desc= descripción de la entidad<dd4></dd>
    <li>
    <a>	
 	<strong>Respuesta</strong>: envía un código 200 y un mensaje de inserción correcta cuando el servidor ha respondido y notifica con otro mensaje en caso de error.
      </a>
      </li>
      <dd>Resultado:</dd>
     </ul>
<pre>
200 exito, la entidad se ha insertado
</pre>
</li>
</ul>
</div>
<div class="post">
<h2 class="title"></h2>

<div class="entry">
<h2>Modificar descripción de la entidad</h2>
 <p> Descripción: Actualiza el nombre de la entidad mediante la clave descrita en la URL </p>
	 <ul><li><strong>URL</strong>: http://santaellagarciaarturo.sun.fire/api/entidad/clave?desc="nombreEntidad"</li>
 <li><strong>Método HTTP</strong>: PUT</li>
 	<li><strong>Parámetros</strong>: <dd>clave entidad= clave de la entidad</dd4></li>
 <li><strong>Respuesta</strong>: envía un código 200 y un mensaje de actualización correcta cuando el servidor ha respondido y notifica con otro mensaje en caso de error.
      </li>
      <dd>Resultado:</dd>
     </ul>
<pre>
404 la clave de la entidad no existe
500 no se puede acceder a la base de datos
200 la entidad fue actualizada con exito

</pre>
<h2 class="title"></h2>
<div class="entry">
<h2>Eliminar un indicador</h2>
 <p> Descripción: Elimina un indicador mediante el "id_indicador" </p>
	 <ul><li><strong>URL</strong>: http://santaellagarciaarturo.sun.fire/api/indicador/id</li>
 <li><strong>Método HTTP</strong>: DELETE</li>
 	<li><strong>Parámetros</strong>: <dd>id_indicador=id del indicador</dd4></li>
 <li><strong>Respuesta</strong>: envía un código 200 y un mensaje de eliminación correcta cuando el servidor ha respondido y notifica con otro mensaje en caso de error.
      </li>
      <dd>Resultado:</dd>
     </ul>
<pre>
404 el indicador no existe
500 no se puede acceder a la base de datos
200 el indicador fue eliminado con exito
</pre>
</li>
</ul>
</div>
</body>
</html>
