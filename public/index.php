<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>API</title>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<div id="header">
		
			<h1> <a href="/api">API de PIB</a></h1>
			<p> <a>Herramientas de Programación Web</a></p>

	</div>
	<div id="page">
		<div id="page-bgtop"><div class="inner_copy"></div>
			<div id="page-bgbtm">
				<div id="content">
					<div class="post">
						<h2 class="title">Introducción</h2>
						<div class="entry">
							<p>La API nos pertime acceder a los datos nacionales que contine el INEGI acerca del Producto Interno Bruto, correspondientes de 1994 al 2011. Con ella el usuario podrá acceder a diferentes recursos.

Para información sobre cómo hacer uso de la API, por favor acceda a la <a href="/doc/">Documentación</a></p>

<h3> <a href="/api">Recursos</a></h3>

<div class="post">
						<h2 class="title">Integrantes del equipo</h2>
						<div class="entry">
							<p>
								<li>
								Cruz Ventura Ricardo       
								</li>
                                <dd><a>N°C: 10160813</a></dd>
                                <li>
                                Francisco Martínez Walter	
                                </li>	
                               <dd><a>N°C: 09161169</a></dd>         
                               <li>
                                Lavariega Sanchez Luis Mauricio
                                </li>
                                <dd><a>N°C: 10160850</a></dd>
                                <li>
                                 Martínez Bautista Francisco Javier 
                                </li>
                                <dd><a>N°C: 10161591</a></dd>
                                <li>
                                Martínez Martínez Manuel
                                </li>
                                <dd><a>N°C: 10160861</a></dd>
                                <li>
                                Ruiz García Edgardo Jesús
                                </li>
                                <dd><a>N°C:10161715</a></dd> 
                                <li>
                                Santaella García Arturo
                                </li>                              
                                <dd><a >N°C: 09161292</a></dd>
                                
							</p>
						</div>
					</div>
</body>
</html>
