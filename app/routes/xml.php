<?php

function id_invalido($id_i) {
  if (strlen($id_i) != 10) {
    return true;
  }
  /* Â¿Algo mÃ¡s por validar? */
}

$app->get('/pib', function() use($app) {
  /* SerÃ¡ usado por app/templates/xml.php */
  $datos = array(
    'books' => array(
    )
  );

  /* SelecciÃ³n en la bd */
  $books = $app->db->query("select i.desc_indicador, p.cantidad, p.unidad_medida, f.anio,
(select t.tema_nivel from pib p inner join tema t on t.id_tema=p.id_tema1 where t.id_tema=p.id_tema1 limit 1) as tema_nivel_1,
(select t.tema_nivel from pib p inner join tema t on t.id_tema=p.id_tema2 where t.id_tema=p.id_tema2 limit 1) as tema_nivel_2,
(select t.tema_nivel from pib p inner join tema t on t.id_tema=p.id_tema3 where t.id_tema=p.id_tema3 limit 1) as tema_nivel_3
from pib p 
inner join indicador i on i.id_indicador=p.id_indicador
inner join fecha f on f.id_fecha=p.id_fecha
inner join entidad e on e.cve_entidad=i.cve_entidad
where f.anio like '%'");
  if (!$books) {
    /* Hubo algÃºn error al seleccionar los datos de la bd */
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informaciÃ³n de los 
libros.");
  }

  /* En caso de exito, se genera el documento XML */
  foreach ($books as $book) {
    $datos['books'][] = $book;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php', $datos);
});

$app->get('/pib/:id', function($id) use($app) {
  /* SerÃ¡ usado por app/templates/xml.php */
  $data = array(
    'books' => array(
    )
  );

if (id_invalido($id)) {
    $app->halt(400, "Error: El id '$id' no cumple con el formato correcto.");
  }

  /* SelecciÃ³n en la bd */
$id_indicador=$id;
	$consulta="select i.desc_indicador, p.cantidad, p.unidad_medida, f.anio,
(select t.tema_nivel from pib p inner join tema t on t.id_tema=p.id_tema1 where t.id_tema=p.id_tema1 limit 1) as tema_nivel_1,
(select t.tema_nivel from pib p inner join tema t on t.id_tema=p.id_tema2 where t.id_tema=p.id_tema2 limit 1) as tema_nivel_2,
(select t.tema_nivel from pib p inner join tema t on t.id_tema=p.id_tema3 where t.id_tema=p.id_tema3 limit 1) as tema_nivel_3
from pib p 
inner join indicador i on i.id_indicador=p.id_indicador
inner join fecha f on f.id_fecha=p.id_fecha
where i.id_indicador like '".$id_indicador."%'";
  $qry = $app->db->prepare("".$consulta);

//$qry->bindParam(':id_indicador', $id);

  if ($qry->execute() === false) {
    /* AlgÃºn error se presentÃ³ con la bd */
    $app->halt(500, "Error: No se ha podido encontrar el indicador con id '$id'.");
  }

  if (!$qry) {
    /* Hubo algÃºn error al seleccionar los datos de la bd */
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informaciÃ³n.");
  }
	if ($qry->rowCount() ==0) {
    /* No se borrÃ³ ningÃºn libro en la bd */
    $app->halt(404, "Error: El indicador con id '$id' no existe.");
  }

  /* En caso de exito, se genera el documento XML */
  foreach ($qry as $res) {
    $data['books'][] = $res;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('pib_id_anio.php', $data);
});


